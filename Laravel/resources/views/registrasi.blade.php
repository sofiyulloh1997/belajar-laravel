<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Buat Halaman FROM</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up From</h3>
    <form action="/kirim" method="POST">
        @csrf
      <label for="username">First name :</label><br />
      <input type="text" name="fname" /><br /><br />
      <label for="username">Last name :</label><br />
      <input type="text" name="lname" /><br /><br />
      <label for="username">Gender :</label><br /><br />
      <input type="radio" />male <br />
      <input type="radio" />female <br />
      <input type="radio" />other <br /><br />
      <label>Nationality :</label>
      <br /><br />
      <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="jawatimur">Jawa Timur</option>
        <option value="jawabarat">Jawa Barat</option>
        <option value="Jawatengah">Jawa Tengah</option>
      </select>
      <br /><br />
      <label>Language Spoken :</label><br /><br />
      <input type="checkbox" />Bahasa Indonesia <br />
      <input type="checkbox" />English <br />
      <input type="checkbox" />Other <br />
      <br /><br />
      <label>Bio :</label><br /><br />
      <textarea name="bioa" id="10" cols="30" rows="10"></textarea>
      <br /><br>
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
